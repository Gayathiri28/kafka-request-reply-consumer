package com.ztech.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationConsumerApplication.class, args);
	}

}
