package com.ztech.kafka.couchbaseConfig;

import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractReactiveCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableReactiveCouchbaseRepositories;

@Configuration
@EnableReactiveCouchbaseRepositories
public class Config extends AbstractReactiveCouchbaseConfiguration {
	@Override
	protected List<String> getBootstrapHosts() {
		// return Collections.singletonList("http://127.0.0.1:8091");
		return Collections.singletonList("http://127.0.0.1");
	}

	@Override
	protected String getBucketName() {
		return "demo";
	}

	@Override
	protected String getBucketPassword() {
		return "password";
	}

	@Override
	protected String getUsername() {
		return "Administrator";
	}

}
