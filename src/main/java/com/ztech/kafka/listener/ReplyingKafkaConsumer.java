
package com.ztech.kafka.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.SuccessCallback;

import com.ztech.kafka.avro.Address;
import com.ztech.kafka.avro.ApplicationError;
import com.ztech.kafka.avro.ApplicationResponse;
import com.ztech.kafka.avro.ApplicationStatus;
import com.ztech.kafka.avro.Student;
import com.ztech.kafka.model.AddressModel;
import com.ztech.kafka.model.StudentModel;
import com.ztech.kafka.repository.StudentRepository;

import reactor.core.publisher.Mono;

@Component
public class ReplyingKafkaConsumer {

	@Autowired
	StudentRepository studentRepository;

	@KafkaListener(topics = "${kafka.topic.request-topic}")
	@SendTo
	public ApplicationResponse listen(Student request) throws InterruptedException {
		
		
		StudentModel studentModel = new StudentModel();
		studentModel.setFirstName(request.getFirstName().toString());
		studentModel.setLastName(request.getLastName().toString());
		studentModel.setEmail(request.getEmail().toString());
		studentModel.setFileURL(request.getFileURL().toString());
		AddressModel addressModel = new AddressModel();
		Address address = request.getAddress();
		addressModel.setHouseAddress(address.getHouseAddress().toString());
		addressModel.setCity(address.getCity().toString());
		addressModel.setState(address.getState().toString());
		addressModel.setStreetAddress(address.getStreetAddress().toString());
		addressModel.setPostalCode(address.getPostalCode().toString());
		addressModel.setCountry(address.getCountry().toString());
		studentModel.setAddress(addressModel);
		
		
		

		 Mono<ApplicationResponse> mono = studentRepository.existsById(String.valueOf(studentModel.getEmail()))
				.map(response -> {
					ApplicationResponse appRes = new ApplicationResponse();
					if (response == true) {
						ApplicationError appError = new ApplicationError();
						appError.setErrorCode("1");
						appError.setErrorDescription("Duplicate email.. choose another email id");
						appRes.setIsSuccess(false);
						appRes.setResponseBody(appError);
						return appRes;
					} else {
						studentRepository.save(studentModel).subscribe();
						ApplicationStatus appStatus = new ApplicationStatus();
						appStatus.setStatus("Processing");
						appStatus.setDescription(
								"Your application is being processed..We will notify you once its approved");
						appRes.setIsSuccess(true);
						appRes.setResponseBody(appStatus);
					}
					return appRes;
				});
		return mono.block();  
	} 
		
	}

