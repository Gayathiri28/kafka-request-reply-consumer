package com.ztech.kafka.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;

@Document
public class AddressModel {
	@Field
	String houseAddress;
	@Field
	String streetAddress;
	@Field
	String city;
	@Field
	String state;
	@Field
	String postalCode;
	@Field
	String country;

	public String getHouseAddress() {
		return houseAddress;
	}

	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return "Address [houseAddress=" + houseAddress + ", streetAddress=" + streetAddress + ", city=" + city
				+ ", state=" + state + ", postalCode=" + postalCode + ", country=" + country + "]";
	}

	public AddressModel() {

	}

}
