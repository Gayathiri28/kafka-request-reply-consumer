package com.ztech.kafka.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document
public class StudentModel {
	@Id
	String email;

	@Field
	String firstName;

	@Field
	String lastName;

	@Field
	AddressModel address;

	@Field
	String fileURL;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public AddressModel getAddress() {
		return address;
	}

	public void setAddress(AddressModel address) {
		this.address = address;
	}

	public String getFileURL() {
		return fileURL;
	}

	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	@Override
	public String toString() {
		return "Student [email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + ", address="
				+ address + ", fileURL=" + fileURL + "]";
	}

	public StudentModel() {

	}

}
