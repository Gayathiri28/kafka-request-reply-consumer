package com.ztech.kafka.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.ztech.kafka.avro.Student;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import com.ztech.kafka.model.*;

public interface StudentRepository extends ReactiveCrudRepository<com.ztech.kafka.model.StudentModel, String> {
	Mono<com.ztech.kafka.model.StudentModel> findById(String email);

	Mono<Void> deleteById(String email);

	Mono<Boolean> existsById(CharSequence charSequence);

	Mono<com.ztech.kafka.model.StudentModel> save(Student newStudent);

	Flux<com.ztech.kafka.model.StudentModel> findAll();

}